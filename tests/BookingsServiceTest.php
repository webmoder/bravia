<?php namespace Tests;

use PHPUnit\Framework\TestCase;
use Storage\InMemoryStorage;

class BookingsServiceTest extends TestCase
{
    /**
     * @var \BookingService
     */
    private $service;
    /**
     * @var \BookingRepository
     */
    private $repository;
    /**
     * @var InMemoryStorage
     */
    private $storage;

    public function setUp()
    {
        $this->storage = new InMemoryStorage();
        $this->repository = new \BookingRepository($this->storage);
        $this->service = new \BookingService($this->repository);
    }

    public function testCanBookingRoomOnDates()
    {
        $result = $this->service->toBook(
            1,
            1,
            \DateTime::createFromFormat('Y-m-d', '2019-03-01'),
            \DateTime::createFromFormat('Y-m-d', '2019-03-05'),
            500
        );

        $this->assertTrue($result);
        $this->assertFalse($this->service->isFreeDate(1, \DateTime::createFromFormat('Y-m-d', '2019-03-02')));
    }

    public function testCantBookingRoomOnCrossDates()
    {

        $booking1 = $this->service->toBook(
            1,
            1,
            \DateTime::createFromFormat('Y-m-d', '2019-03-01'),
            \DateTime::createFromFormat('Y-m-d', '2019-03-05'),
            500
        );

        $booking2 = $this->service->toBook(
            1,
            1,
            \DateTime::createFromFormat('Y-m-d', '2019-03-04'),
            \DateTime::createFromFormat('Y-m-d', '2019-03-08'),
            500
        );

        $booking3 = $this->service->toBook(
            1,
            1,
            \DateTime::createFromFormat('Y-m-d', '2019-02-27'),
            \DateTime::createFromFormat('Y-m-d', '2019-03-02'),
            500
        );

        $this->assertTrue($booking1);
        $this->assertFalse($booking2);
        $this->assertFalse($booking3);
    }
}