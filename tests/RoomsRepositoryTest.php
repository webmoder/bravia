<?php

namespace Tests;

use Storage\InMemoryStorage;
use PHPUnit\Framework\TestCase;
use Room;
use RoomsRepository;

class RoomsRepositoryTest extends TestCase
{

    public function testCanSaveARoomInStorage()
    {
        $room = new Room(1, 1);
        $storage = new InMemoryStorage;
        $repository = new RoomsRepository($storage);

        $repository->save($room);

        $this->assertTrue($storage->has($room->getId()));
        $this->assertEquals($repository->findById($room->getId()), $room);
    }
}