<?php

namespace Tests;

use Booking;
use BookingRepository;
use Storage\InMemoryStorage;
use PHPUnit\Framework\TestCase;

class BookingsRepositoryTest extends TestCase
{

    public function testCanSaveABookingsInStorage()
    {
        $booking1 = new Booking(
            1,
            1,
            \DateTime::createFromFormat('Y-d-m', '2019-03-13'),
            \DateTime::createFromFormat('Y-d-m', '2019-03-20'),
            1000
        );
        $booking2 = new Booking(
            1,
            2,
            \DateTime::createFromFormat('Y-d-m', '2019-03-21'),
            \DateTime::createFromFormat('Y-d-m', '2019-03-24'),
            500
        );
        $storage = new InMemoryStorage;
        $repository = new BookingRepository($storage);

        $repository->save($booking1);
        $repository->save($booking2);

        $this->assertTrue($storage->has($booking1->getId()));
        $this->assertContains($booking1, $repository->findByRoomId(1));
        $this->assertContains($booking2, $repository->findByRoomId(1));
    }
}