<?php

namespace Concepts;

interface Storage
{
    public function set(Identifiable $identifiable);

    public function get($id);

    public function has($id): bool;

    public function reduce(callable $callable, $initial = null): array;
}