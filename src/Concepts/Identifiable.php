<?php

namespace Concepts;

interface Identifiable
{
    public function getId();
}