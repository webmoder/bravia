<?php

use Concepts\Identifiable;

class Booking implements Identifiable
{
    /**
     * @var int
     */
    public $roomId;

    /**
     * @var int
     */
    public $personId;

    /**
     * @var DateTime
     */
    public $startDate;

    /**
     * @var DateTime
     */
    public $endDate;

    /**
     * @var int
     */
    public $price;

    public function __construct(int $roomId, int $personId, DateTime $startDate, DateTime $endDate, int $price)
    {
        $this->roomId = $roomId;
        $this->personId = $personId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->price = $price;
    }

    public function getId()
    {
        return "{$this->roomId}.{$this->startDate->format('Y-m-d')}";
    }
}