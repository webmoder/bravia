<?php

use Concepts\Storage;

class BookingRepository
{
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param int $roomId
     * @return Booking[]
     */
    public function findByRoomId(int $roomId): array
    {
        return $this->storage->reduce(function ($bookings, $value) use ($roomId) {
            $booking = $this->mapBookingFromStorageValue($value);

            if (($booking->roomId ?? null) === $roomId) {
                $bookings[] = $booking;
            }

            return $bookings;
        }, []);
    }

    public function save(Booking $booking): void
    {
        $this->storage->set($booking);
    }

    /**
     * @param mixed $value
     * @return Booking|null
     */
    private function mapBookingFromStorageValue($value): ?Booking
    {
        return is_a($value, Booking::class) ? $value : null;
    }
}