<?php

use Concepts\Storage;

class RoomsRepository
{
    /**
     * @var Storage
     */
    private $storage;

    /**
     * RoomsRepository constructor.
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;

    }

    public function save(Room $room): void
    {
        $this->storage->set($room);
    }

    public function findById(int $id): ?Room
    {
        if ($this->storage->has($id)) {
            return $this->mapRoomFromStorageValue($this->storage->get($id));
        }

        return null;
    }

    private function mapRoomFromStorageValue($value): ?Room
    {
        return is_a($value, Room::class) ? $value : null;
    }
}