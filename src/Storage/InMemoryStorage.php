<?php

namespace Storage;

use Concepts\Identifiable;
use Concepts\Storage;

class InMemoryStorage implements Storage
{
    private $storage = [];

    public function set(Identifiable $identifiable)
    {
        $this->storage[$identifiable->getId()] = $identifiable;
    }

    public function get($id)
    {
        return $this->storage[$id] ?? null;
    }

    public function has($id): bool
    {
        return isset($this->storage[$id]);
    }

    public function reduce(callable $callable, $initial = null): array
    {
        return array_reduce($this->storage, $callable) ?? [];
    }
}