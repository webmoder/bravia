<?php

class BookingService
{
    /**
     * @var BookingRepository
     */
    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function toBook(int $roomId, int $personId, DateTime $startDate, DateTime $endDate, int $price): bool
    {
        $startDate->setTime(0,0);
        $endDate->setTime(0,0);
        if (!$this->isFreeDatePeriod($roomId, $startDate, $endDate)) {
            return false;
        }

        $booking = new Booking($roomId, $personId, $startDate, $endDate, $price);

        $this->repository->save($booking);

        return true;
    }

    public function isFreeDate(int $roomId, DateTime $date): bool
    {
        foreach ($this->repository->findByRoomId($roomId) as $booking) {
            if ($booking->startDate <= $date && $booking->endDate >= $date) {
                return false;
            }
        }

        return true;
    }

    private function isFreeDatePeriod(int $roomId, DateTime $startDate, DateTime $endDate): bool
    {
        foreach ($this->repository->findByRoomId($roomId) as $booking) {
            if (
                ($startDate < $booking->startDate && $endDate > $booking->startDate) ||
                ($startDate < $booking->endDate && $endDate > $booking->endDate)
            ) {
                return false;
            }
        }

        return true;
    }
}