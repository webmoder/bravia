<?php

use Concepts\Identifiable;

class Room implements Identifiable
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $size;

    public function __construct(int $id, int $size)
    {
        $this->id = $id;
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }
}